﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollPlanes : MonoBehaviour {

	//Variables 
	[SerializeField] private float ObjectSpeed;
	[SerializeField] private float initialplanePos;
	private BoxCollider planeCollider;
	private Transform planeTransform;
	private float planeHorizontalLength;

	private float resetPos;
	private float startPos;
	// Use this for initialization
	void Start () 
	{
		
		planeCollider = GetComponent<BoxCollider>();
		planeTransform = GetComponent<Transform>();
		
		planeHorizontalLength = planeTransform.localScale.x * planeCollider.size.x;
		resetPos = -planeHorizontalLength + initialplanePos;
		startPos = resetPos + 2f * planeHorizontalLength;

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(Gamemanager.instance.GameStarted)
		{
			if(!Gamemanager.instance.GameOver)
			{
				transform.position -= new Vector3(ProgressManager.FactorSpeed * ObjectSpeed * Time.deltaTime, 0, 0);

				if(transform.localPosition.x < -planeHorizontalLength + initialplanePos)
				{
					Vector3 newPos = new Vector3(startPos, transform.position.y,transform.position.z);
					transform.position = newPos;
				}
			}
		}
	}
	
}
