﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DefenseBraumShield : MonoBehaviour {

    [SerializeField] private float duracion;
    [SerializeField] private float cooldown;

    [SerializeField] private GameObject botonCarrusel;

    private float timerDuracion;
    private float timerCooldown;

    private float timerCDShow;

    // Use this for initialization
    void Start () {
        timerDuracion = duracion;
        timerCooldown = 0;
        DesactivarDefensa();
    }
	
	// Update is called once per frame
	void Update () {
        if (!Gamemanager.instance.GameStarted)
        {
            return;
        }

        if (Gamemanager.instance.GameOver)
        {
            return;
        }

        if (timerCooldown > 0)
        {
            timerCooldown -= Time.deltaTime;
            timerCDShow = timerCooldown % 60;

            botonCarrusel.GetComponentInChildren<Text>().text = timerCDShow.ToString("00");
            if (timerCooldown <= 0)
            {
                botonCarrusel.GetComponent<Button>().interactable = true;
                botonCarrusel.GetComponentInChildren<Text>().text = "";
            }
        }

        if (gameObject.GetComponent<MeshRenderer>().enabled != true)
        {
            return;
        }

        if (timerDuracion > 0)
        {
            timerDuracion -= Time.deltaTime;

            if (timerDuracion <= 0f)
            {
                timerDuracion = duracion;
                DesactivarDefensa();
            }
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Attack")
        {
            Destroy(collision.gameObject);
            // TODO: Agregar particulas, sonido y mas
            DesactivarDefensa();
        }
    }

    public void ActivarDefensa()
    {
        if (timerCooldown <= 0)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            gameObject.GetComponent<CapsuleCollider>().enabled = true;
            timerCooldown = cooldown;
        }
    }

    void DesactivarDefensa()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
    }

    public void ReiniciaTimers()
    {
        DesactivarDefensa();
        timerDuracion = duracion;
        timerCooldown = 0;
        botonCarrusel.GetComponent<Button>().interactable = true;
        botonCarrusel.GetComponentInChildren<Text>().text = "";
    }
}
