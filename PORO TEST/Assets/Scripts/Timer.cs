﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

	[SerializeField] private Text counterText;


	private float meters;


    
    // Use this for initialization
	void Start () {
		counterText = GetComponent<Text>() as Text;
	}
	
	// Update is called once per frame
	void Update () {
        if (!Gamemanager.instance.GameStarted)
        {
            return;
        }
        if (Gamemanager.instance.GameOver == true)
        {
            return;
        }

        UpdateTimerUI();
	}

    public void UpdateTimerUI()
    {
        meters += Time.deltaTime * ProgressManager.FactorSpeed;
        counterText.text = (int)meters + " mts";
    }

    public void IniciaTimer()
    {
        meters = 0f;
        counterText.text = (int)meters + " mts";
    }

    public void ReiniciaTimeScale()
    {

    }

    public void BorraTimer()
    {
        Time.timeScale = 0f;
        meters = 0f;
        counterText.text = "";
    }
}
