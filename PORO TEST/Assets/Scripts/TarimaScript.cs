﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TarimaScript : MonoBehaviour {

	// Use this for initialization
	[SerializeField] private float ObjectSpeed;
	private Vector3 spawnPos;

	void Start () {
		spawnPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (Gamemanager.instance.GameStarted)
        {
            if (!Gamemanager.instance.GameOver)
            {
                transform.position -= new Vector3(ObjectSpeed * Time.deltaTime, 0, 0);
            }
        }
		
		if(transform.position.x <= -5f)
		{
			transform.position = new Vector3(-5f, 0, 0);
		}
    }

	    public void ReiniciarTarima()
    {
		transform.position = spawnPos;
    }



}
