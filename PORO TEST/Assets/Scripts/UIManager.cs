﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager instance = null;

    public GameObject PopUpReintentar;
    public GameObject PopUpSalir;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void  Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            MostrarPopUpSalir();
        }
    }

    public void MostrarPopUpReintentar()
    {
        PopUpReintentar.SetActive(true);
    }

    public void MostrarPopUpSalir()
    {
        if (!PopUpSalir.activeSelf)
        {
            Time.timeScale = 0f;
            PopUpSalir.SetActive(true);
        }
    }

    public void CerrarPopUpSalir()
    {
        if (PopUpSalir.activeSelf)
        {
            PopUpSalir.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void CerrarAplicacion()
    {
        //if (Application.platform == RuntimePlatform.Android)
        //{
        //    AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
        //    activity.Call<bool>("moveTaskToBack", true);
        //}
        //else
        //{        
            Gamemanager.instance.ReiniciaScene();
            Application.Quit();
        //}
    }
}
