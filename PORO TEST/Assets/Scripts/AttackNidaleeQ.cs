﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackNidaleeQ : MonoBehaviour {

	[SerializeField] private float ObjectSpeed;
	[SerializeField] private float RotationSpeed;
    [SerializeField] private float deathForceX;

    private Rigidbody rigidBody;




    // Use this for initialization
    void Start () {
        rigidBody = GetComponent<Rigidbody>();
    }
	

	// Update is called once per frame
    protected virtual void Update () 
	{
        if (!Gamemanager.instance.GameStarted)
        {
            return;
        }

        if (Gamemanager.instance.GameOver)
        {
            return;
        }
        transform.Translate(ProgressManager.FactorSpeed * Vector3.forward * Time.deltaTime * ObjectSpeed);
		transform.Rotate (new Vector3 (0, 0, RotationSpeed * Time.deltaTime));

		if(transform.position.x <= -5f)
		{
			Destroy(gameObject);
		}
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            rigidBody.AddForce (new Vector2 (deathForceX, 0), ForceMode.Impulse);
            Destroy(gameObject);
            // TODO: Agregar particulas, sonido y mas
        }
    }
}
