﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressManager : MonoBehaviour {

	public static ProgressManager instance = null;
	private float actualTime; //tiempo del juego
	[SerializeField] private float timeInterval; //tiempo intervalos de progresión
	private float interval = 1f;
	private float timeIncrement;
	private float factorSpeed = 1f;
	[SerializeField] public float incrementFactorSpeed;



	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

	public static float FactorSpeed 
	{
		get
		{
		return instance.factorSpeed;
		} 
	} 

	// Update is called once per frame
	void Update () 
	{
		if(Gamemanager.instance.GameStarted)
		{
			if(!Gamemanager.instance.GameOver)
			{
				actualTime += Time.deltaTime;
				timeIncrement = interval*timeInterval;
				if(actualTime >= timeIncrement)
				{
					interval++;
					factorSpeed += incrementFactorSpeed;
				}

			}
		}
	}


	public void resetActualtim(){
		actualTime = 0;
		interval = 1f;
		factorSpeed = 1f;
	}
}
