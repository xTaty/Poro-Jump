﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle2 : RockBasic {

	[SerializeField] private float fallPosition;
	private new Rigidbody rigidbody;
	private Vector3 force;
	private float forceY = -3f;

	// Use this for initialization
	void Start () 
	{
		rigidbody = GetComponentInChildren<Rigidbody>();
	}
	
	// Update is called once per frame
	protected override void Update () {
		base.Update();
		force = new Vector3(0, forceY * ProgressManager.FactorSpeed, 0);
		if(transform.position.x <= fallPosition)
		{
			rigidbody.isKinematic = false;
			//rigidbody.useGravity = true;
			rigidbody.AddForce(force, ForceMode.Acceleration);
		}


	}
}
