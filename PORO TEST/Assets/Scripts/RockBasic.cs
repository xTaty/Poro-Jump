﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBasic : MonoBehaviour {

	[SerializeField] private float ObjectSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	protected virtual void Update () 
	{
        if (Gamemanager.instance.GameStarted)
        {
            if (!Gamemanager.instance.GameOver)
            {
                transform.position -= new Vector3(ProgressManager.FactorSpeed * ObjectSpeed * Time.deltaTime, 0, 0);
            }
        }
		
		if(transform.position.x <= -5f)
		{
			Destroy(gameObject);
		}
    }
}
