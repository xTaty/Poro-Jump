﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class Gamemanager : MonoBehaviour {

	//Variable Gamemanager
	public static Gamemanager instance = null;
	//Variables de estado
	private bool gameOver = false;
	private bool gameStarted = false;
    private bool gameRestarted = false;
    private float seconds;

	//getters

	public bool GameOver {
		get { return gameOver; }
	}

	public bool GameStarted {
		get { return gameStarted; }
	}

    public bool GameRestarted
    {
        get { return gameRestarted; }
    }
	
	// Use this for initialization

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

	void Start () {
        Time.timeScale = 0;
	}
	
	// Update is called once per frame

    public void PlayerCollided()
    {
        gameOver = true;
        Time.timeScale = 1f;
        UIManager.instance.MostrarPopUpReintentar();
    }

	public void EnterGame()
	{
		gameStarted = true;
        Time.timeScale = 1f;
	}

	public void GameRestart()
    {
        gameOver = false;
        gameRestarted = true;
    }

    public void SetgameRestarted(bool estado)
    {
        gameRestarted = estado;
    }

    public void ReiniciaVariablesParaMenuPrincipal()
    {
        gameOver = false;
        gameStarted = false;
        gameRestarted = false;
        Time.timeScale = 0;
    }

    public void ReiniciaScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
