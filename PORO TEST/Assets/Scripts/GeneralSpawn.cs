﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralSpawn : MonoBehaviour
{

	//
	//atributos de las oleadas
	[SerializeField] private Transform[] spawnPoints;
	[SerializeField] private float spawnTime;
	[SerializeField] private float spawnTimeRock;
	[SerializeField] private float waveWaitTime;
	[SerializeField] private int rocksCount;
	[SerializeField] private int spearCount;
	[SerializeField] private Transform poro_target;
	[SerializeField] private Transform spawnSpear;
	private Quaternion topRotation, botRotation;
	

	//
	//Lista de rocas y lanzas
	[SerializeField] private GameObject[] rocks;
	[SerializeField] private GameObject[] attacks;
	

	//
	//variables de la Corutina y lista de objetos
	private IEnumerator coroutine;
    private List<GameObject> estalagmitas;
	private List<GameObject> estalagFlappy;
	private List<GameObject> estalagRota;
	private List<GameObject> nidaQ;
	private bool coroutineOn;
	private bool attacking = false;
	private int i,j;

	void Start ()
	{
		//
		//Creación de las listas
		estalagmitas = new List<GameObject>();
		estalagFlappy = new List<GameObject>();
		estalagRota = new List<GameObject>();
		nidaQ = new List <GameObject>();

		//Llamada de la rutina
		coroutine =Spawner();
		StartCoroutine(coroutine);
		coroutineOn = true;
	}

	void Update()
	{
		if(Gamemanager.instance.GameOver == true)
		{
			StopCoroutine(coroutine);
			coroutineOn = false;
		}
		if(!Gamemanager.instance.GameOver && Gamemanager.instance.GameStarted && !coroutineOn)
		{
			StartCoroutine(coroutine);
			coroutineOn = true;
		}
	}

	IEnumerator Spawner()
	{
		int spawnIndex;

		while(true)
		{
			if(attacking == false)
			{
				for(i=1; i<= rocksCount; i++)
				{
					//
					//Declaración variables de posición de estalagmita.
					int objectIndex = Random.Range(0, rocks.Length);

					//
					//CASO ESTALAGMITA SIMPLE

					if(objectIndex == 0)
					{
						spawnIndex = Random.Range(0, spawnPoints.Length);

						if(spawnIndex == 0 )
						{
							topRotation = Quaternion.Euler(new Vector3(0,Random.Range(-45f,180f),180));
							instanciarEstalagmitas(objectIndex, spawnIndex, topRotation);
							yield return new WaitForSeconds (spawnTime);
						}
						if(spawnIndex == 1)
						{
							botRotation = Quaternion.Euler(new Vector3(0,Random.Range(-45,180f),0));
							instanciarEstalagmitas(objectIndex, spawnIndex, botRotation);
							yield return new WaitForSeconds (spawnTime);
						}
					}

					//
					//CASO ROCA GIGANTE

					if (objectIndex == 1)
					{
						spawnIndex = Random.Range(0, spawnPoints.Length);
						if(spawnIndex == 0)
						{
							topRotation = Quaternion.Euler(new Vector3(0,Random.Range(-45f,45f),180));
							instanciarEstalagmitas(objectIndex, spawnIndex, topRotation);
							yield return new WaitForSeconds(spawnTimeRock);
						}
						if (spawnIndex == 1)
						{
							botRotation = Quaternion.Euler(new Vector3(0,Random.Range(-45,45f),0));
							instanciarEstalagmitas(objectIndex, spawnIndex, botRotation);
							yield return new WaitForSeconds(spawnTimeRock);
						}
					}

					//
					//CASO ESTALAGMITA FLAPPY BIRD

					if(objectIndex == 2)
					{
						Vector3 randomHeight = new Vector3(8.25f, Random.Range(-0.66f,0.58f), transform.position.z);
						instanciarEstFlappy(objectIndex, randomHeight, transform.rotation);
						yield return new WaitForSeconds(spawnTime);
					}

					//
					//CASO ESTALAGMITA DEBIL

					if(objectIndex == 3)
					{
						instanciarEstRota(objectIndex);
						yield return new WaitForSeconds(spawnTime);
					}

				}
			attacking = true;
			yield return new WaitForSeconds(waveWaitTime);
			}


			if (attacking == true)
			{
				for(j=1; j <= spearCount; j++)
				{
					
					int objectIndex = Random.Range(0, attacks.Length);
					
					if(objectIndex == 0)
					{

						Vector3 randomHeight = new Vector3(8.25f, Random.Range(-1.68f,2.5f),0);
						spawnSpear.transform.position = randomHeight;
						Vector3 dir = poro_target.transform.position - spawnSpear.transform.position;
						Quaternion lookRotation = Quaternion.LookRotation(dir);
						instanciarNidaQ(objectIndex, randomHeight, lookRotation);
					}
					

					yield return new WaitForSeconds(spawnTime);
				}
				yield return new WaitForSeconds(waveWaitTime);
				attacking = false;
			}
		}
	}


	//
	//Funciones de instanciación y agregación a las listas.
	//
	private void instanciarEstalagmitas(int objectIndex, int spawnIndex, Quaternion rotacion)
	{
		estalagmitas.Add(Instantiate(rocks[objectIndex], spawnPoints[spawnIndex].position, rotacion));
	}

	private void instanciarEstFlappy(int objectIndex, Vector3 randomHeight, Quaternion rotacion)
	{
		estalagFlappy.Add(Instantiate(rocks[objectIndex], randomHeight, transform.rotation));
	}

	private void instanciarEstRota(int objectIndex)
	{
		estalagRota.Add(Instantiate(rocks[objectIndex], spawnPoints[0].position, spawnPoints[0].rotation));
	}
	private void instanciarNidaQ(int objectIndex, Vector3 randomHeight, Quaternion lookRotation)
	{
		nidaQ.Add(Instantiate(attacks[objectIndex], randomHeight, lookRotation));
	}


	//
	//Elimina objetos al apretar restart o la X
	public void destruirTodos()
	{
		foreach(GameObject e in estalagmitas)
		{
			Destroy (e);
		}

		foreach(GameObject d in estalagFlappy)
		{
			Destroy(d);
		}

		foreach(GameObject f in estalagRota)
		{
			Destroy(f);
		}

		foreach(GameObject n in nidaQ)
		{
			Destroy(n);
		}
	}

	//
	//Resetea la corutina al apretar restart o la X
	public void restartCoroutine ()
	{
		i = 0;
		j = 0;
		attacking = false;
	}
}
