﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poro : MonoBehaviour 
{
	[SerializeField] private float jumpForce;
	[SerializeField] private float deathForceX;
	[SerializeField] private float deathForceY;

    //sounds
    [SerializeField] private AudioClip jumpfx;
    [SerializeField] private AudioClip shutdownFX;

    //Animator
    private Animator anim, anim_tongue;

    //AudioSource
    private AudioSource audioSource;
    
    private bool oneTime = true;
    private Quaternion rotacionInicial;
    private Vector3 posicionInicial;

	private Rigidbody rigidBody;
	private bool jump = false;

	// Use this for initialization
	void Start () 
	{
        anim = GetComponent<Animator> ();
		rigidBody = GetComponent<Rigidbody> ();
        audioSource = GetComponent<AudioSource> ();

        rotacionInicial = rigidBody.rotation;
        posicionInicial = rigidBody.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
        if (Gamemanager.instance.GameStarted && rigidBody.useGravity != true)
        {
            rigidBody.useGravity = true;
        }

        if (!Gamemanager.instance.GameOver)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (Time.timeScale > 0)
                {
                    anim.Play("Poro_tap");
                    audioSource.PlayOneShot (jumpfx);
                    jump = true;
                }
            }
        }
	}


	void FixedUpdate()
	{
		if (jump == true) 
		{
		jump = false;
		rigidBody.velocity = new Vector2 (0, 0);
		rigidBody.AddForce (new Vector2 (0, jumpForce), ForceMode.Impulse);
		}
         

	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Floor") 
		{
			//rigidBody.detectCollisions = false;
            rigidBody.velocity = new Vector2(0,0);
            rigidBody.angularVelocity = Vector3.zero;
            if(oneTime == true)
            {
                audioSource.PlayOneShot(shutdownFX);
                oneTime = false;
            }
            Gamemanager.instance.PlayerCollided();
            
		}
		
		if (collision.gameObject.tag == "Rock")
		{
			rigidBody.AddForce (new Vector2 (deathForceX, deathForceY), ForceMode.Impulse);
            if(oneTime == true)
            {
                audioSource.PlayOneShot(shutdownFX);
                oneTime = false;
            }
			//rigidBody.detectCollisions = false;
            Gamemanager.instance.PlayerCollided();
        }

        if (collision.gameObject.tag == "Attack")
        {
            rigidBody.AddForce(new Vector2(deathForceX, deathForceY), ForceMode.Impulse);
            if(oneTime == true)
            {
                audioSource.PlayOneShot(shutdownFX);
                oneTime = false;
            }
            //rigidBody.detectCollisions = false;
            Gamemanager.instance.PlayerCollided();
        }
    }

    public void SetJump(bool condicion)
    {
        jump = condicion;
    }

    public void ReiniciarPoroRestart()
    {
        rigidBody.transform.SetPositionAndRotation(posicionInicial, rotacionInicial);
        rigidBody.detectCollisions = true;
        rigidBody.velocity = new Vector2(0, 0);
        rigidBody.angularVelocity = Vector3.zero;
        oneTime = true;
        Gamemanager.instance.SetgameRestarted(false);
        SetJump(true);
        
    }

    public void ReiniciarPoroMenuPrincipal()
    {
        rigidBody.transform.SetPositionAndRotation(posicionInicial, rotacionInicial);
        rigidBody.detectCollisions = true;
        rigidBody.velocity = new Vector2(0, 0);
        rigidBody.angularVelocity = Vector3.zero;
        rigidBody.useGravity = false;
        oneTime = true;
    }
}
